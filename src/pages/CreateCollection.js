import React, { useContext, useState, useEffect } from "react";
import { AuthContext } from "../context/Auth.Context"
import { useHttp } from "../hooks/http.hook"
import { useHistory } from "react-router-dom"



export const CreatePage = () => {

    const auth = useContext(AuthContext)
    const { request} = useHttp()
    const history = useHistory()

    const [form, setForm] = useState({
        name: "",
        description: "",
        topic: "",

        field_text_1: "",
        field_text_2: "",
        field_text_3: "",

        field_bool_1: "",
        field_bool_2: "",
        field_bool_3: "",

        field_str_1: "",
        field_str_2: "",
        field_str_3: "",

        field_int_1: "",
        field_int_2: "",
        field_int_3: "",

        field_date_1: "",
        field_date_2: "",
        field_date_3: ""

    })



    useEffect(() => {
        window.M.updateTextFields()
    }, [])


    const changeHandler = event => {

        setForm({ ...form, [event.target.name]: event.target.value })

    }

    const createHandler = async () => {
        try {

            

            const data = await request("/api/link/generate", "POST", {
                "name": form.name, "description": form.description, "topic": form.topic, "fields":[

                    { "name": form.field_text_1, "type": "text", "number":"field_text_1" },
                    { "name": form.field_text_2, "type": "text", "number": "field_text_2" },
                    { "name": form.field_text_3, "type": "text", "number": "field_text_3" },
                    { "name": form.field_int_1, "type": "number", "number": "field_int_1" },
                    { "name": form.field_int_2, "type": "number", "number": "field_int_2" },
                    { "name": form.field_int_3, "type": "number", "number": "field_int_3" },
                    { "name": form.field_str_1, "type": "text", "number": "field_str_1" },
                    { "name": form.field_str_2, "type": "text", "number": "field_str_2" },
                    { "name": form.field_str_3, "type": "text", "number": "field_str_3" },
                    { "name": form.field_bool_1, "type": "checkbox", "number": "field_bool_1" },
                    { "name": form.field_bool_2, "type": "checkbox", "number": "field_bool_2" },
                    { "name": form.field_bool_3, "type": "checkbox", "number": "field_bool_3" },
                    { "name": form.field_date_1, "type": "date", "number": "field_date_1" },
                    { "name": form.field_date_2, "type": "date", "number": "field_date_2" },
                    { "name": form.field_date_3, "type": "date", "number": "field_date_3" },



                    
                    
                    ]}, {
                Authorization: `Bearer ${auth.token}`
            })

            

            history.push(`/${data.link._id}`)

        } catch (e) { }
    }


    return (
        <div className="row">
            <div className="col s8 offset-s2">
                <div className="input-field">
                    <input
                    placeholder="Input name"
                    id="name"
                    type="text"
                    name="name"
                    value={form.name}
                    onChange={changeHandler}
                    />
                    <label htmlFor="name">Name</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input description"
                        id="description"
                        type="text"
                        name="description"
                        value={form.description}
                        onChange={changeHandler}
                    />
                    <label htmlFor="description">Description</label>

                </div>


                <div className="input-field">
                    <input
                        placeholder="Input topic"
                        id="topic"
                        type="text"
                        name="topic"
                        value={form.topic}
                        onChange={changeHandler}
                    />
                    <label htmlFor="topic">Topic</label>
                </div>


                <div className="input-field">
                    <input
                        placeholder="Input field`s name (text)"
                        id="field_text_1"
                        type="text"
                        name="field_text_1"
                        value={form.field_text_1}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_text_1">Text field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (text)"
                        id="field_text_2"
                        type="text"
                        name="field_text_2"
                        value={form.field_text_2}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_text_2">Text field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (text)"
                        id="field_text_3"
                        type="text"
                        name="field_text_3"
                        value={form.field_text_3}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_text_3">Text field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (int)"
                        id="field_int_1"
                        type="text"
                        name="field_int_1"
                        value={form.field_int_1}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_int_1">Int field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (int)"
                        id="field_int_2"
                        type="text"
                        name="field_int_2"
                        value={form.field_int_2}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_int_2">Int field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (int)"
                        id="field_int_3"
                        type="text"
                        name="field_int_3"
                        value={form.field_int_3}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_int_3">Int field</label>

                </div>


                <div className="input-field">
                    <input
                        placeholder="Input field`s name (string)"
                        id="field_str_1"
                        type="text"
                        name="field_str_1"
                        value={form.field_str_1}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_str_1">Str field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (string)"
                        id="field_str_2"
                        type="text"
                        name="field_str_2"
                        value={form.field_str_2}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_str_2">Str field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (string)"
                        id="field_str_3"
                        type="text"
                        name="field_str_3"
                        value={form.field_str_3}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_str_3">Str field</label>

                </div>



                <div className="input-field">
                    <input
                        placeholder="Input field`s name (date)"
                        id="field_date_1"
                        type="text"
                        name="field_date_1"
                        value={form.field_date_1}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_date_1">Date field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (date)"
                        id="field_date_2"
                        type="text"
                        name="field_date_2"
                        value={form.field_date_2}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_date_2">Date field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (date)"
                        id="field_date_3"
                        type="text"
                        name="field_date_3"
                        value={form.field_date_3}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_date_3">Date field</label>

                </div>



                <div className="input-field">
                    <input
                        placeholder="Input field`s name (bool)"
                        id="field_bool_1"
                        type="text"
                        name="field_bool_1"
                        value={form.field_bool_1}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_bool_1">Bool field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (bool)"
                        id="field_bool_2"
                        type="text"
                        name="field_bool_2"
                        value={form.field_bool_2}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_bool_2">Bool field</label>

                </div>

                <div className="input-field">
                    <input
                        placeholder="Input field`s name (bool)"
                        id="field_bool_3"
                        type="text"
                        name="field_bool_3"
                        value={form.field_bool_3}
                        onChange={changeHandler}
                    />
                    <label htmlFor="field_bool_3">Bool field</label>

                </div>


                
                <div className="card-action">



                    <button className="btn yellow darken-4" style={{ marginRight: 10 }}
                        
                        onClick={createHandler}
                    >Create</button>
                </div>



            </div>
        </div>
        )
}