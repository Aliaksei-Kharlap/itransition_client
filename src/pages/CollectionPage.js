import React, { useCallback, useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import { Loader } from "../components/Loader";
import { LinkCard } from "../components/LinkCard";
import { AuthContext } from "../context/Auth.Context";
import { useHttp } from "../hooks/http.hook";


export const CollectionPage = () => {

    const { token } = useContext(AuthContext)
    const linkId = useParams().id
    const { request, loading } = useHttp()
    const [coll, setColl] = useState(null)


    const getLink = useCallback(async () => {
        try {

            const fetched = await request(`/api/link/such/${linkId}`, "GET", null, {
                Authorization: `Bearer ${token}`
            });

            await setColl(fetched)


            return null


        } catch (e) {
            console.log(e)
        }


    }, [token, request, linkId])

    
    useEffect(() => {

        getLink()
        console.log(coll)

    }, [getLink])

    if (loading) {
        return <Loader />

    }

    return (
        <>
            { !loading && <LinkCard coll={coll} />}
        </>
    )
}