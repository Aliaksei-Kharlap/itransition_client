import React, { useContext, useState, useEffect,useCallback } from "react";
import { AuthContext } from "../context/Auth.Context"
import { useHttp } from "../hooks/http.hook"
import { useHistory, useParams } from "react-router-dom"
import { Loader } from "../components/Loader"



export const CreateItems = () => {

    const auth = useContext(AuthContext)
    const { request, loading } = useHttp()
    const history = useHistory()
    const linkId = useParams().id

    const [names, setNames] = useState([])


    const [form, setForm] = useState({
        name: "",
        collectionId: { linkId },

        field_text_1: "",
        field_text_2: "",
        field_text_3: "",

        field_bool_1: "",
        field_bool_2: "",
        field_bool_3: "",

        field_str_1: "",
        field_str_2: "",
        field_str_3: "",

        field_int_1: "",
        field_int_2: "",
        field_int_3: "",

        field_date_1: "",
        field_date_2: "",
        field_date_3: ""

    })

    const [collec, setCollec] = useState([])

    const fetchLinks = useCallback(async () => {

        try {

            const data = await request(`/api/link/${linkId}`, "GET", null, {
                Authorization: `Bearer ${auth.token}`
            });
            

            await setCollec(data);
           
            const namess = [] 

            data.fields.map(i => {
                namess.push(i.name)
                return 0
            })


            console.log(namess)

            await setNames(namess)

          
            

         

        } catch (e) { console.log(e) }

    }, [auth, request, linkId])


    useEffect(() => {
        window.M.updateTextFields()
    }, [])

    useEffect( () => {
        
            fetchLinks()

    }, [])




    



    const changeHandler = event => {

        setForm({ ...form, [event.target.name]: event.target.value })

    }

    const createHandler = async () => {
        try {

            

            const data = await request("/api/link/items", "POST", {
                "name": form.name, "collectionId": form.collectionId, "fields": [

                    { "value": form.field_text_1, "name": "hello" },
                    { "value": form.field_text_2, "name": names[1] },
                    { "value": form.field_text_3, "name": names[2] },
                    { "value": form.field_int_1, "name": names[3] },
                    { "value": form.field_int_2, "name": names[4] },
                    { "value": form.field_int_3, "name": names[5] },
                    { "value": form.field_str_1, "name": names[6] },
                    { "value": form.field_str_2, "name": names[7] },
                    { "value": form.field_str_3, "name": names[8] },
                    { "value": form.field_bool_1, "name": names[9] },
                    { "value": form.field_bool_2, "name": names[10] },
                    { "value": form.field_bool_3, "name": names[11] },
                    { "value": form.field_date_1, "name": names[12] },
                    { "value": form.field_date_2, "name": names[13] },
                    { "value": form.field_date_3, "name": names[14] },





                ]
            }, {
                Authorization: `Bearer ${auth.token}`
            })



            history.push(`/collection/${linkId}`)

        } catch (e) { }
    }

    if (loading) {
        return <Loader />

    }


    return (
    

    
        <div className="row">
            <h1 className="col s8 offset-s2">Create item</h1>
            <div className="col s8 offset-s2">
                <div className="input-field">
                    <input
                        placeholder="Input item`s name"
                        id="name"
                        type="text"
                        name="name"
                        value={form.name}
                        onChange={changeHandler}
                    />
                    <label htmlFor="name">Name</label>

                </div>

                {collec.fields?.map(obj => {
                      

                    if (!!obj.name) {

                        return (

                            <div className="input-field">
                                <input
                                    placeholder={obj.name}
                                    id={obj.name}
                                    type={obj.type}
                                    name={obj.number}
                                    value={form[obj.number]}
                                    onChange={changeHandler}
                                />
                                <label htmlFor={obj.name}>Input {obj.name}</label>

                            </div>


                        )
                    } else { return null}
                    })}

                <div className="card-action">



                    <button className="btn yellow darken-4" style={{ marginRight: 10 }}

                        onClick={createHandler}
                    >Create</button>
                </div>



            </div>
        </div>
        )
  
}