import React from "react";
import { Switch, Route, Redirect, Router } from "react-router-dom";
import { CollectionPage } from "./pages/CollectionPage"
import { CreatePage } from "./pages/CreateCollection"
import { YourCollections } from "./pages/YourCollection"
import { AuthPage } from "./pages/AuthPage"
import { CreateItems } from "./pages/CreateItems"


export const useRoutes = isAuthenticated => {

    if (isAuthenticated) {

        return (

            
            <Switch>

                <Route path="/collections" exact>
                    <YourCollections />
                </Route>

                <Route path="/createcol" exact>
                    <CreatePage />
                </Route>

                <Route path="/collection/:id" exact>
                    <CollectionPage />
                </Route>

                <Route path="/items/:id" exact>
                    <CreateItems />
                </Route>

                <Redirect to="/collections" />

                </Switch >
                
        )

    }

    return (
            <Switch>

                <Route path="/" exact>
                    <AuthPage />
                </Route>

                <Redirect to="/" />

            </Switch >
        )

}