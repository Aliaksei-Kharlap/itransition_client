import { useCallback } from "react"




export const useMessale = () => {

    return useCallback(text => {
        if (window.M && text) {
            window.M.toast({html: text})
        }
    }, [])

}