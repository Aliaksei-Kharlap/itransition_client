import React from "react"
import { Link } from "react-router-dom"

export const LinksList = ({ links }) => {

    if (!links) {
        return <p className="center">No collections yet {links} fgfg</p>
    }


    return (

        <div>
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Topic</th>
                </tr>
            </thead>

            <tbody>
                {links.map(link => {
                    return (
                        <tr key={ link._id}>
                            <td>{link.name }</td>
                            <td>{link.description}</td>
                            <td>{link.topic}</td>
                            <td>
                                <Link to={`/items/${link._id}`}>Create item</Link>
                            </td>
                            <td>
                                <Link to={`/collection/${link._id}`}>Open</Link>
                            </td>
                        </tr>
                        
                        )
                })}
                
            </tbody>
        </table>
        
        </div>
        
        )
}