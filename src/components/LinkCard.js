import React from "react"

export const LinkCard = ({ coll }) => {

	{
		if (!coll) { return <p>Ni items yet</p>} }


	return (

		<>
			<h2>Collection: {coll[0].name}</h2>
			<div>
			<p>Collection`s name: { coll[0].name }</p>
			<p>Collection`s description:  {coll[0].description}</p>
			<p>Collection`s topic:  {coll[0].topic}</p>
			</div>

			<div>
				<h2>Items:</h2>
				{coll[1].map((item) => {
					return (

						<div>
							<h3>Item`s name: {item.name}</h3>
							{item.fields.map((field) => {
								if (field.name) {
								return (
									<div>
										<p>{field.name}: {field.value}</p>

									</div>
									
									)
								} else return null
							})}


						</div>
					

				)


				})}


			</div>


		</>
		
		
		)


}