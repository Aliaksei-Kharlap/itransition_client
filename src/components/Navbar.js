import React, { useContext } from "react";
import { useHistory, NavLink } from "react-router-dom";
import { AuthContext } from "../context/Auth.Context";




export const Navbar = () => {
    const auth = useContext(AuthContext)
    const history = useHistory()

    const logoutHandler = event => {
        event.preventDefault()
        auth.logout()
        history.push('/')
    }


	return (
	
	<nav>
    <div className="nav-wrapper">
            <a href="/" className="brand-logo">Logo</a>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
 
                    <li><NavLink to="/createcol">�������</NavLink></li>
                    <li><NavLink to="/collections">�������</NavLink></li>
                    <li><a href="/" onClick={logoutHandler}>Logout</a></li>
                </ul>
    </div>
  </nav>
	

	)

}